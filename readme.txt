To localize the UI of the Android Apps form Spartan Entertainment (https://play.google.com/store/apps/developer?id=Spartan+Entertainment) : 

1) If a folder for your language doesn't exist, copy the default english folder (it's name is "values") and rename it to "values-lang" folder where lang is the 2 letter id for your language (fr for french, de for german,...) 

2) In the strings.xml file, translate all strings that are between the <string> and </string> tags or between the <item> and </item> tags.
Don't touch the stuff between the <xliff:g> and </xliff:g> tags or the comments between <!-- and --> : it is here to provide context information to help with the translation quality and shouldn't be translated

When confused/at a loss what to do, look into the english & french resources

A) Tech-savy/technical people can clone the repository, make changes and make pull requests to get their changes merged with the main repository
B) Or, you can send me your translated strings.xml file by email to [olivier dot binda at wanadoo dot fr]


